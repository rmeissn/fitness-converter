# Fitness Converter

...

## How to get started

1. Clone the repository via `git clone ...`.
2. Change into the directory and install all dependencies via `npm install`
3. Transpile and run the project via `npm start`. The built SPA should be automatically opened in your browser. If this isn't the case, navigate to [http://localhost:8080](http://localhost:8080)

The fronted is now started in development mode and both hot-reload in case any code was changed. You may run `npm run lint` to lint all code via ESLint.

## Production Environment
Either use the included dockerfile to build a docker image and the included docker-compose.yml files to start it up, or run `npm run prod` to build and serve the project locally.

Look at the dockerfile and docker-compose.yml files to gain insights into manual deployment.

## Development
All SPA related code resides in the `src` directory and is separated into different folders via the default Vue.js project structure (MVC). The easiest way to get around is to look at a desired view and to look for components of this view. Some views are reusable by providing different parameters to them. Look at the router file to gain insights into reused routes and provided parameters.
