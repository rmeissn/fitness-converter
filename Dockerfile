# See https://github.com/rmeissn/tech4comp-dockerfiles for the base image description
FROM node:lts-alpine
LABEL maintainer="Roy Meissner <meissner@informatik.uni-leipzig.de>"

ARG BUILD_ENV=LOCAL
ENV VIRTUAL_PORT=${VIRTUAL_PORT:-80}

RUN mkdir /nodeApp
WORKDIR /nodeApp

# ---------------- #
#   Installation   #
# ---------------- #

RUN npm install -g --no-optional --production serve

COPY ./ ./
RUN if [ "$BUILD_ENV" != "CI" ] ; then rm -R node_modules ; npm install ; fi
RUN npm run build

# ----------------- #
#   Configuration   #
# ----------------- #

# ----------- #
#   Cleanup   #
# ----------- #

RUN rm -R node_modules && npm cache clean --force

# -------- #
#   Run!   #
# -------- #

CMD serve -s -n -C -l $VIRTUAL_PORT /nodeApp/dist
