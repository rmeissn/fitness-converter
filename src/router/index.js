import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/OPHealthConverter',
      name: 'OPHealthConverter',
      component: () => import(/* webpackChunkName: "OPHealth" */ '@/views/OPHealthConverter.vue')
    },
    {
      path: '/viewer',
      name: 'Viewer',
      component: () => import(/* webpackChunkName: "viewer" */ '@/views/Viewer.vue'),
      props: true
    }
  ]
})
