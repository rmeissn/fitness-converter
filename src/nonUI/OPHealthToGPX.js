import { buildGPX, StravaBuilder } from 'gpx-builder'
import { isEmpty } from 'lodash'
import { resolveSportType } from '@/nonUI/OPHealthToTCX'
const { Point, Metadata, Track, Segment } = StravaBuilder.MODELS

async function convertOPHealthFileToGPX (file) {
  const trackPoints = file.detailData.map(data => {
    const point = file.gpsData.find(el => el.timeStamp === data.timeStamp)
    let trackPoint
    if (!isEmpty(point)) {
      const extension = {
        ele: data.elevation / 10,
        time: new Date(point.timeStamp * 1000),
        speed: point.speed,
        distance: data.distance
      }
      if (data.heartRate !== 0)
        extension.hr = data.heartRate
      trackPoint = new Point(point.latitude, point.longitude, extension)
    } else
      trackPoint = new Point(0, 0, {
        time: new Date(data.timeStamp * 1000),
        hr: data.heartRate
      })

    return trackPoint
  }).filter(a => a !== undefined)

  const gpxData = new StravaBuilder()
  const tracks = [new Track([new Segment(trackPoints)], {
    type: resolveSportType(file.sportType)
  })]
  gpxData.setTracks(tracks)
  gpxData.setMetadata(new Metadata({
    time: new Date(file.startTime * 1000),
    desc: 'Recorded on OnePlus Watch'
  }))

  return buildGPX(gpxData.toObject())
}

export { convertOPHealthFileToGPX }
