import { TrainingCenterDatabase, Activity, ActivityLap, Track, TrackPoint, Position, HeartRateBpm, TrackPointExtensions, ActivityList, HeartRateInBeatsPerMinute, Device, Version } from 'tcx-builder'
import { isEmpty, first } from 'lodash'

async function convertOPHealthFileToTCX (file) {
  const hasWayPoints = sportTypeHasWayPoints(file.sportType)
  const trackPoints = file.detailData.map((data, index) => {
    const point = file.gpsData.find(el => el.timeStamp === data.timeStamp)
    let trackPoint
    if (!isEmpty(point)) {
      const distance = file.detailData.slice(0, index).map(a => a.distance).reduce((a, b) => a + b, 0)
      trackPoint = new TrackPoint({
        time: new Date(point.timeStamp * 1000),
        position: new Position(point.latitude, point.longitude),
        altitudeMeters: data.elevation / 10,
        distanceMeters: distance || 0,
        heartRateBpm: new HeartRateBpm(data.heartRate),
        // sensorState: 'Present',
        extensions: new TrackPointExtensions({
          Speed: point.speed
        })
      })
    } else
      trackPoint = new TrackPoint({
        time: new Date(data.timeStamp * 1000),
        heartRateBpm: new HeartRateBpm(data.heartRate)
        // sensorState: 'Present'
      })

    if (trackPoint.HeartRateBpm.Value === 0) {
      if (isEmpty(point))
        trackPoint = undefined
      else
        delete trackPoint.HeartRateBpm
    }
    return trackPoint
  }).filter(a => a !== undefined)

  const sortedHeartRate = file.detailData.map(a => a.heartRate).filter(a => a !== 0).sort((a, b) => (a < b) ? 1 : 0)

  let lap
  if (hasWayPoints)
    lap = {
      Calories: file.totalCalories,
      DistanceMeters: file.totalDistance,
      Intensity: 'Active',
      TotalTimeSeconds: file.totalTime,
      TriggerMethod: 'Manual',
      MaximumSpeed: file.maxSpeed,
      MaximumHeartRateBpm: new HeartRateInBeatsPerMinute({ value: first(sortedHeartRate) }),
      AverageHeartRateBpm: new HeartRateInBeatsPerMinute({ value: file.avgHeartRate }),
      Track: new Track({ trackPoints })
    }
  else
    lap = {
      Calories: file.totalCalories,
      DistanceMeters: 0,
      Intensity: 'Active',
      TotalTimeSeconds: file.totalTime,
      MaximumHeartRateBpm: new HeartRateInBeatsPerMinute({ value: first(sortedHeartRate) }),
      AverageHeartRateBpm: new HeartRateInBeatsPerMinute({ value: file.avgHeartRate }),
      Track: new Track({ trackPoints })
    }
  const myLap = new ActivityLap(new Date(file.startTime * 1000), lap)

  const tcxActivity = new Activity(resolveSportType(file.sportType), {
    Id: new Date(file.startTime * 1000),
    Laps: [myLap],
    Creator: new Device({
      name: 'OnePlus Watch',
      unitId: 0,
      productID: 301,
      version: new Version(0, 0, 0, 0)
    })
  })
  const activityList = new ActivityList({ activity: [tcxActivity] })

  const tcxObj = new TrainingCenterDatabase({ activities: activityList })

  return tcxObj.toXml()
}

// Commented ones are codes from Google Fit. Uncommented ones are codes from OP Health
// Strings are from Google Fit, see https://developers.google.com/fit/rest/v1/reference/activity-types
const opHealthFitnessTypes = new Map([
  // 9: 'Aerobics',
  // 119: 'Archery',
  // 10: 'Badminton',
  // 11: 'Baseball',
  // 12: 'Basketball',
  // 13: 'Biathlon',
  [3, 'Biking'],
  // 14: 'Handbiking',
  // 15: 'Mountain biking',
  // 16: 'Road biking',
  // 17: 'Spinning',
  // 18: 'Stationary biking',
  // 19: 'Utility biking',
  // 20: 'Boxing',
  // 21: 'Calisthenics',
  // 22: 'Circuit training',
  // 23: 'Cricket',
  // 113: 'Crossfit',
  // 106: 'Curling',
  [407, 'Dancing'],
  // 102: 'Diving',
  // 117: 'Elevator',
  // 25: 'Elliptical',
  // 103: 'Ergometer',
  // 118: 'Escalator',
  // 26: 'Fencing',
  // 27: 'Football (American)',
  // 28: 'Football (Australian)',
  // 29: 'Football (Soccer)',
  // 30: 'Frisbee',
  // 31: 'Gardening',
  // 32: 'Golf',
  // 122: 'Guided Breathing',
  // 33: 'Gymnastics',
  // 34: 'Handball',
  // 114: 'HIIT',
  [506, 'Hiking'],
  // 36: 'Hockey',
  // 37: 'Horseback riding',
  // 38: 'Housework',
  // 104: 'Ice skating',
  // 0: 'In vehicle',
  [35, 'Interval Training'], // used for Freestyle Training
  // 39: 'Jumping rope',
  // 40: 'Kayaking',
  // 41: 'Kettlebell training',
  // 42: 'Kickboxing',
  // 43: 'Kitesurfing',
  // 44: 'Martial arts',
  // 45: 'Meditation',
  // 46: 'Mixed martial arts',
  [108, 'Other'],
  // 47: 'P90X exercises',
  // 48: 'Paragliding',
  // 49: 'Pilates',
  // 50: 'Polo',
  // 51: 'Racquetball',
  // 52: 'Rock climbing',
  // 53: 'Rowing',
  // 54: 'Rowing machine',
  // 55: 'Rugby',
  // 8: 'Running',
  // 56: 'Jogging',
  // 57: 'Running on sand',
  // 58: 'Running (treadmill)',
  // 59: 'Sailing',
  // 60: 'Scuba diving',
  // 61: 'Skateboarding',
  // 62: 'Skating',
  // 63: 'Cross skating',
  // 105: 'Indoor skating',
  // 64: 'Inline skating',
  // 65: 'Skiing',
  // 66: 'Back-country skiing',
  // 67: 'Cross-country skiing',
  // 68: 'Downhill skiing',
  // 69: 'Kite skiing',
  // 70: 'Roller skiing',
  // 71: 'Sledding',
  // 73: 'Snowboarding',
  // 74: 'Snowmobile',
  // 75: 'Snowshoeing',
  // 120: 'Softball',
  // 76: 'Squash',
  // 77: 'Stair climbing',
  // 78: 'Stair-climbing machine',
  // 79: 'Stand-up paddleboarding',
  // 6: 'Still (not moving)',
  // 80: 'Strength training',
  // 81: 'Surfing',
  // 82: 'Swimming',
  // 84: 'Swimming (open water)',
  // 83: 'Swimming (swimming pool)',
  // 85: 'Table tennis (ping pong)',
  // 86: 'Team sports',
  // 87: 'Tennis',
  // 5: 'Tilting (sudden device gravity change)',
  // 88: 'Treadmill (walking or running)',
  // 4: 'Unknown',
  // 89: 'Volleyball',
  // 90: 'Volleyball (beach)',
  // 91: 'Volleyball (indoor)',
  // 92: 'Wakeboarding',
  [1, 'Walking']
  // 93: 'Walking (fitness)',
  // 94: 'Nording walking',
  // 95: 'Walking (treadmill)',
  // 116: 'Walking (stroller)',
  // 96: 'Waterpolo',
  // 97: 'Weightlifting',
  // 98: 'Wheelchair',
  // 99: 'Windsurfing',
  // 100: 'Yoga',
  // 101: 'Zumba'
])

function resolveSportType (numericSportType) {
  const result = opHealthFitnessTypes.get(numericSportType)
  return (result === undefined) ? 'Other' : result
}

function sportTypeHasWayPoints (numericSportType) {
  return [1, 3, 506].includes(numericSportType)
}

export { convertOPHealthFileToTCX, resolveSportType, sportTypeHasWayPoints }
