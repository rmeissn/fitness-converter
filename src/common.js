module.exports = {
  loadFileContent: async function (file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.onload = async function (e) {
        resolve(e.target.result)
      }
      reader.readAsBinaryString(file)
    })
  },

  timeout: async function (timeInMS) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve()
      }, timeInMS)
    })
  },

  isTextFile (file) {
    return file.name.endsWith('.txt') && (file.type === 'text/plain' || file.type === '')
  },

  isJSONFile (file) {
    return file.name.endsWith('.json') && (file.type === 'application/json' || file.type === '')
  },

  isGPXFile (file) {
    return file.name.endsWith('.gpx') && (file.type === 'application/gpx+xml' || file.type === '')
  },

  isTCXFile (file) {
    return file.name.endsWith('.tcx') && (file.type === 'application/vnd.garmin.tcx+xml' || file.type === '')
  },

  isFITFile (file) {
    return file.name.endsWith('.fit') && (file.type === 'application/octet-stream' || file.type === '')
  }

}
